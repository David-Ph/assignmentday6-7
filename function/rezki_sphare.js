const index = require("../index");

function inputSphare() {
  index.rl.question("Input radius : ", (radius) => {
    if (index.isNotValidInput(radius)) {
      console.log("Please input valid number");
      inputSphare();
    } else {
      console.log(`Sphare: ${sphareVol(radius)}`);
      index.main();
    }
  });
}

function sphareVol(radius) {
  return (4 / 3) * Math.PI * radius ** 3;
}

module.exports = {
  inputSphare,
};
