//author : Kimbrian Marshall

//import index
const index = require("../index");

let cone = (radius, height) => {
  return (Math.PI * radius * radius * height) / 3;
};

let input = () => {
  index.rl.question("Please input cone radius: ", (r) => {
    index.rl.question("Please input cone height: ", (h) => {
      if (index.isNotValidInput(r) || index.isNotValidInput(h)) {
        console.log("Please input valid number");
        input();
      } else {
        console.log("Cone volume is ", cone(r, h));
        index.main();
      }
    });
  });
};

module.exports = { input };
