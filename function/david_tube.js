//author : David

const index = require("../index");

// function to calculate tube volume
function calcTubeVol(radius, height) {
  const pi = 3.14;
  const vol = pi * (radius * radius) * height;
  //   returns only 2 decimal places
  return parseFloat(vol.toFixed(2));
}

function input() {
  index.rl.question("Please input tube radius: ", (radius) => {
    index.rl.question("Please input tube height: ", (height) => {
      if (!index.isNotValidInput(radius) && !index.isNotValidInput(height)) {
        //   if input is valid
        console.log("Tube volume is ", calcTubeVol(radius, height));
        index.main();
      } else {
        console.log(`Please input valid number`);
        input();
      }
    });
  });
}

module.exports = { input };
